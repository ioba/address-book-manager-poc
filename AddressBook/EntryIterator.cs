﻿using System;
using System.Reflection;
using AddressBook.InternalEngine;

namespace AddressBook
{
    
    public class EntryIterator
    {
        private IAddressBookStoreEngine store = null;
        private int currentIndex = 0;

        public delegate AddressBookEntryDocument ByPosiotionFetcher(int index);

        private  ByPosiotionFetcher getByPosition;

        
        public AddressBookEntry FirstItem
        {
            get
            {
                currentIndex = 0;
                return getByPosition(currentIndex).MapToAddressBookEntry();
            }
        }

        public AddressBookEntry NextItem
        {
            get
            {
                currentIndex += 1;
                return IsDone == false ? getByPosition(currentIndex).MapToAddressBookEntry() : null;
            }
        }

        public AddressBookEntry CurrentItem => getByPosition(currentIndex).MapToAddressBookEntry();

        private bool IsDone => currentIndex >= store.Count();

        public EntryIterator(IAddressBookStoreEngine store, ByPosiotionFetcher fetcher)
        {
            this.store = store;
            getByPosition +=  (ByPosiotionFetcher) fetcher;

        }
    }
}


  



  