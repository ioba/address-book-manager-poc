﻿using System;

namespace AddressBook.Exceptions
{
    public class InvalidPhoneNumberFormat : Exception
    {
        public InvalidPhoneNumberFormat()
        {   
        }

        public InvalidPhoneNumberFormat(string message)
            : base(message)
        {
        }

        public InvalidPhoneNumberFormat(string message, Exception inner)
            : base(message, inner)
        {
            
        }
    }
}