﻿using System;

namespace AddressBook.Exceptions
{
    public class PhoneNumberNullOrEmptyException : Exception
    {
        public PhoneNumberNullOrEmptyException()
        {   
        }

        public PhoneNumberNullOrEmptyException(string message)
            : base(message)
        {
        }

        public PhoneNumberNullOrEmptyException(string message, Exception inner)
            : base(message, inner)
        {
            
        }
    }
}