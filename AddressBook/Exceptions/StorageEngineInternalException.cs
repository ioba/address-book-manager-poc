﻿using System;

namespace AddressBook.Exceptions
{
    public class StorageEngineInternalException : Exception
    {
        public StorageEngineInternalException()
        {   
        }

        public StorageEngineInternalException(string message)
            : base(message)
        {
        }

        public StorageEngineInternalException(string message, Exception inner)
            : base(message, inner)
        {
            
        }
    }
}