﻿using System;

namespace AddressBook.Exceptions
{
    public class LastNameNullOrEmptyException : Exception
    {
        public LastNameNullOrEmptyException()
        {   
        }

        public LastNameNullOrEmptyException(string message)
            : base(message)
        {
        }

        public LastNameNullOrEmptyException(string message, Exception inner)
            : base(message, inner)
        {
            
        }
    }
}