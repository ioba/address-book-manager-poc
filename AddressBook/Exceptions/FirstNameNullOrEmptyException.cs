﻿using System;

namespace AddressBook.Exceptions
{
    public class FirstNameNullOrEmptyException : Exception
    {
        public FirstNameNullOrEmptyException()
        {   
        }

        public FirstNameNullOrEmptyException(string message)
            : base(message)
        {
        }

        public FirstNameNullOrEmptyException(string message, Exception inner)
            : base(message, inner)
        {
            
        }
        
    }
}