﻿using System;

namespace AddressBook.Exceptions
{
    public class AddressBookEntryNullIdException: Exception
    {
        public AddressBookEntryNullIdException()
        {   
        }

        public AddressBookEntryNullIdException(string message)
            : base(message)
        {
        }

        public AddressBookEntryNullIdException(string message, Exception inner)
            : base(message, inner)
        {
            
        } 
    }
}