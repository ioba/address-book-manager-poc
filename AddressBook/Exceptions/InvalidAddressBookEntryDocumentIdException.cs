﻿using System;

namespace AddressBook.Exceptions
{
    public class InvalidAddressBookEntryDocumentIdException : Exception
    {
        public InvalidAddressBookEntryDocumentIdException()
        {   
        }

        public InvalidAddressBookEntryDocumentIdException(string message)
            : base(message)
        {
        }

        public InvalidAddressBookEntryDocumentIdException(string message, Exception inner)
            : base(message, inner)
        {
            
        }
    }
}