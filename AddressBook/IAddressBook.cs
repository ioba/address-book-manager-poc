﻿using System;

namespace AddressBook
{
    
    
    /// <summary>
    /// IAddressBook defines the basic available functions (public Api) that would be available
    /// for the end user of our lib use.
    /// The core functionality would be implemented by the engine class <see cref="AddresBookEngine"/>.
    /// </summary>
    public interface IAddressBook
    {
       
        /// <summary>
        /// Creates a new address book entry in our engine.
        /// </summary>
        /// <param name="firstName">The first name of the entry (is mandatory and should be provided).</param>
        /// <param name="lastName">The last name of the entry (is mandatory and should be provided).</param>
        /// <param name="type">The type of the entry, it should be of type <see cref="IAddressBookType"/>. </param>
        /// <param name="phoneNumber">The phone number of the entry.</param>
        /// <returns> The created  Address Book Entity <see cref="AddressBookEntity"/></returns>
        AddressBookEntry CreateEntry(String firstName, String lastName, AddressBookEntryType type,String phoneNumber);

        AddressBookEntry UpdateEntry(AddressBookEntry addressBookEntry);

        void DeleteEntry(AddressBookEntry addressBookEntry);

        void PersistAddressBook();

    }
}