﻿using System;
using System.Security.Cryptography.X509Certificates;

namespace AddressBook
{
    public class AddressBookEntry
    {
        /// <summary> The Unique Identifier of the entry.</summary>
        public string Id { get; }
        
        /// <summary>The First Name of the entry.</summary>
        public string FirstName { get; }
        
        /// <summary>The Last Name of the entry.</summary>
        public string LastName { get; }
        
        /// <summary>The Type of the entry.</summary>
        public AddressBookEntryType Type { get; }
        
        /// <summary>The Phone Number of the entry.</summary>
        public string PhoneNumber { get; }


        public AddressBookEntry(String id, string firstName, string lastName, AddressBookEntryType type, string phoneNumber)
        {
            Id = id;
            FirstName = firstName;
            LastName = lastName;
            Type = type;
            PhoneNumber = phoneNumber;
        }
    }
}