﻿using System;
using AddressBook.Exceptions;
using AddressBook.InternalEngine;

namespace AddressBook
{
    public class AddressBookEngine :  IAddressBook    
    {
        
        
        private readonly IAddressBookStoreEngine _storeEngine;
        private readonly IGuidService _guidService;
        
        private IAddressBookStoreEngine StoreEngine => _storeEngine;
        private IGuidService GuidService => _guidService;

        /// <inheritdoc />
        /// <summary>
        /// Creates a new address book entry in our engine.
        /// </summary>
        /// <param name="firstName">The first name of the entry (is mandatory and should be provided).</param>
        /// <param name="lastName">The last name of the entry (is mandatory and should be provided).</param>
        /// <param name="type">The type of the entry, it should be of type <see cref="!:IAddressBookType" />. </param>
        /// <param name="phoneNumber">The phone number of the entry.</param>
        /// <returns> The created  Address Book Entity <see cref="!:AddressBookEntity" /></returns>
        /// <exception cref="T:AddressBook.Exceptions.FirstNameNullOrEmptyException"> Thrown when first name 
        /// is null or empty</exception>
        /// <exception cref="T:AddressBook.Exceptions.LastNameNullOrEmptyException"> Thrown when last name 
        /// is null or empty</exception>
        /// <exception cref="T:AddressBook.Exceptions.PhoneNumberNullOrEmptyException"> Thrown when phone number
        /// is null or empty</exception>
        /// <exception cref="T:AddressBook.Exceptions.StorageEngineInternalException"> Thrown when exception is raised 
        /// from underlaying internal storage engine</exception>
        public AddressBookEntry CreateEntry(string firstName, string lastName, AddressBookEntryType type, string phoneNumber)
        {
            if (String.IsNullOrWhiteSpace(firstName))
                throw new FirstNameNullOrEmptyException("First name can not be null or empty");
            
            if (String.IsNullOrWhiteSpace(lastName))
                throw new LastNameNullOrEmptyException("Last name can not be null or empty");
            
            if (String.IsNullOrWhiteSpace(phoneNumber))
                throw new PhoneNumberNullOrEmptyException("Phone number can not be null or empty");


            var id = GuidService.GenerateGuid();
            var addressBookEntryDocument = new AddressBookEntryDocument(id, firstName, lastName, type, phoneNumber);

            AddressBookEntryDocument storedDocument; 
            try
            {
                 storedDocument = _storeEngine.StoreDocument(addressBookEntryDocument);
            }
            catch (Exception ex)
            {
                throw new StorageEngineInternalException("Storage engine internal exceptions.", ex);

            }
            
            return storedDocument.MapToAddressBookEntry();
        }

        public AddressBookEntry UpdateEntry(AddressBookEntry addressBookEntry)
        {
            if (addressBookEntry == null)
                throw  new ArgumentNullException("Can not update a null address book entry");

            if (addressBookEntry.Id == null)
                throw new AddressBookEntryNullIdException("Can not update and address book entry with null id");
            
            AddressBookEntryDocument storedDocument; 
            try
            {
                storedDocument = StoreEngine.StoreDocument(addressBookEntry.MapToAddressBookEntry());
            }
            catch (Exception ex)
            {
                throw new StorageEngineInternalException("Storage engine internal exceptions.", ex);

            }
            
            return storedDocument.MapToAddressBookEntry();
        }

        public void DeleteEntry(AddressBookEntry addressBookEntry)
        {
            throw new System.NotImplementedException();
        }

        public void PersistAddressBook()
        {
            throw new System.NotImplementedException();
        }

        public AddressBookEngine()
        {
            _storeEngine = new AddressBookStoreEngine();
            _guidService = new GuidService();
        }
        
        public AddressBookEngine( IAddressBookStoreEngine storeEngine, IGuidService guidService)
        {
            this._storeEngine = storeEngine;
            this._guidService = guidService;
        }

        public EntryIterator IterateEntriesByFirstName()
        {
            return  new EntryIterator(this._storeEngine,_storeEngine.GetByPositionOnFirstNameIndex);
        }
        
        public EntryIterator IterateEntriesBylastName()
        {
            return  new EntryIterator(this._storeEngine,_storeEngine.GetByPositionOnLastNameIndex);
        }

    }
}