﻿using System;

namespace AddressBook.InternalEngine
{
    public class GuidService : IGuidService
    {
        public Guid GenerateGuid()
        {
            return Guid.NewGuid();
        }
    }
}