﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Data.Common;

namespace AddressBook.InternalEngine
{
    public class StoreIndex
    {
        private readonly SortedList<string, List<IndexKey>> indexData;
        private readonly SortedList<IndexKey,Guid>  sortedIndexData;
        private int currentIndex = 0;
        
        public string FirstItem
        {
            get
            {
                currentIndex = 0;
                return sortedIndexData.Values[currentIndex].ToString();
            }
        }
        
        public string NextItem
        {
            get
            {
                currentIndex++;
                return IsDone == false ? sortedIndexData.Values[currentIndex].ToString() : null;
            }
        }
        
        private bool IsDone
        {
            get
            {
                    return currentIndex >= sortedIndexData.Count;
            }
        }

        public void Add(string key, string guid)
        {
            var indexKey =  new IndexKey(key,guid);
            
            if (indexData.ContainsKey(key))
            {
                indexData[key].Add(indexKey);
            }
            else
            {
                var newList = new List<IndexKey> {indexKey};
                indexData.Add(key,newList);
            }
            
            sortedIndexData.Add(new IndexKey(key,guid),indexKey.Guid);
        }

        public void Remove(string index, string indexRef)
        {
            
            //Todo: Refactor the nested if looks ugly and is not state the intend 
            if (indexData.ContainsKey(index))
            {

                if (indexData?[index].Count > 0)
                    indexData?[index].Remove(new IndexKey(index, indexRef));

                if (indexData?[index].Count == 0)
                    indexData.Remove(index);
            }
            
            sortedIndexData.Remove(new IndexKey(index, indexRef));
        }

        public string GetByPosition(int index)
        {
            return sortedIndexData.Values[index].ToString();
        }

        public StoreIndex()
        {
            indexData = new SortedList<string, List<IndexKey>>();
            sortedIndexData = new SortedList<IndexKey, Guid>( new IndexComparer());
        }
    }


    public  class InternalDoubleIndex
    {
        private int primaryIndex = 0;

        private int secondaryIndex = 0;
       
        public int PrimaryIndex => primaryIndex;

        public int SecondaryIndex => secondaryIndex;

        public void IncreasePrimaryIndex()
        {
            primaryIndex++;
        }

        public void IncreaseSecondaryIndex()
        {
            secondaryIndex++;
        }

        public void Reset()
        {
            primaryIndex = 0;
            secondaryIndex = 0;
        }
    }
    
    public class IndexKey
    {
        public string IndexedField { get; set;}
        public Guid Guid { get; set; }

        public IndexKey(string indexedField, string guid)
        {
            IndexedField = indexedField;
            Guid = Guid.Parse(guid);
        }
       
    }

    public class IndexComparer : IComparer<IndexKey>
    {

        public int Compare(IndexKey x, IndexKey y)
        {
            if (x == null || y == null)
                throw new InvalidOperationException("both of parameters must be not null");

            if (x.IndexedField.CompareTo(y.IndexedField) < 0) return -1;
            if (x.IndexedField.CompareTo(y.IndexedField) > 0) return 1;

            if (x.IndexedField.CompareTo(y.IndexedField) == 0)
            {
                if (x.Guid.CompareTo(y.Guid) < 0) return -1;
                if (x.Guid.CompareTo(y.Guid) > 0) return 1;
            }
            return 0;
        }
    }
}