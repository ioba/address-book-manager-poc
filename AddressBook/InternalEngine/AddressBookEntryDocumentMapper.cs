﻿using System;

namespace AddressBook.InternalEngine
{
    public static class AddressBookEngineMapper
    {
        public static AddressBookEntry MapToAddressBookEntry(this AddressBookEntryDocument document)
        {
            
            return  new AddressBookEntry(document.Id.ToString(),document.FirstName,
                document.LastName, document.Type, document.PhoneNumber);
        }
        
        public static AddressBookEntryDocument MapToAddressBookEntry(this AddressBookEntry entry)
        {
            return  new AddressBookEntryDocument(Guid.Parse(entry.Id),entry.FirstName,
                entry.LastName, entry.Type, entry.PhoneNumber);
        }
    }
}