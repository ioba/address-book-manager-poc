﻿using System;
using System.Text.RegularExpressions;
using AddressBook.Exceptions;

namespace AddressBook.InternalEngine
{
    public class AddressBookEntryDocument
    {
        
        /// <summary> The Unique Identifier of the entry.</summary>
        public Guid Id { get; }
        
        /// <summary>The First Name of the entry.</summary>
        public String FirstName { get; }
        
        /// <summary>The Last Name of the entry.</summary>
        public String LastName { get; }
        
        /// <summary>The Type of the entry.</summary>
        public AddressBookEntryType Type { get; }
        
        /// <summary>The Phone Number of the entry.</summary>
        public String PhoneNumber { get; }

        public AddressBookEntryDocument(Guid id, string firstName, string lastName, AddressBookEntryType type, string phoneNumber)
        {
            
            if (id == null )
                throw new InvalidAddressBookEntryDocumentIdException("Engine can not utialize a document with null id.");
            
            if (String.IsNullOrWhiteSpace(firstName))
                throw new FirstNameNullOrEmptyException("First name can not be null or empty.");
            
            if (String.IsNullOrWhiteSpace(lastName))
                throw new LastNameNullOrEmptyException("Last name can not be null or empty.");
            
            if (String.IsNullOrWhiteSpace(phoneNumber))
                throw new PhoneNumberNullOrEmptyException("Phone number can not be null or empty.");

            if (!IsPhoneNumber(phoneNumber))
                throw new InvalidPhoneNumberFormat("Phone number is not in proper form.");
            
            Id = id;
            FirstName = firstName;
            LastName = lastName;
            Type = type;
            PhoneNumber = phoneNumber;
        }
        
        
        private static bool IsPhoneNumber(string number)
        {
            return Regex.Match(number, @"^(\+[0-9]{9})$").Success;
        }
    }
}