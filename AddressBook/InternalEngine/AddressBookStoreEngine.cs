﻿using System;
using System.Collections.Generic;

namespace AddressBook.InternalEngine
{
    public class AddressBookStoreEngine : IAddressBookStoreEngine
    {

        private readonly SortedList<Guid, AddressBookEntryDocument> _dataStore =
            new SortedList<Guid, AddressBookEntryDocument>();
        
        private readonly StoreIndex _firstNameIndex = new StoreIndex();
        
        private readonly StoreIndex _lastNameIndex = new StoreIndex(); 

        public AddressBookEntryDocument StoreDocument(AddressBookEntryDocument document)
        {
            if (document == null)
                throw new ArgumentNullException("Store engine can not " +
                                                "store a null document");

            AddDocumentToMainStore( document);

            //Todo: Maybe pointless and should be removed, just retutn the passed param.
            var storeDocument = _dataStore[document.Id];
            return storeDocument;

        }


        private void AddDocumentToMainStore(AddressBookEntryDocument document)
        {
            var id = document.Id;

            if (_dataStore.ContainsKey(id))
            {
                RemoveDocumentAndIndexes(document);
            }
            _dataStore.Add(document.Id, document);
            AddDocumentToIndexes(document);
        }


        private void RemoveDocumentAndIndexes(AddressBookEntryDocument document)
        {
            _dataStore.Remove(document.Id);
            _firstNameIndex.Remove(document.FirstName, document.Id.ToString());
            _lastNameIndex.Remove(document.LastName, document.Id.ToString());
        }
        
        private void AddDocumentToIndexes(AddressBookEntryDocument document)
        {
            AddDocumentToFirstNameIndex(document);
            AddDocumentToLastNameIndex(document);
        }

        private void AddDocumentToFirstNameIndex(AddressBookEntryDocument document)
        {
            _firstNameIndex.Add(document.FirstName, document.Id.ToString());
        }

        private void AddDocumentToLastNameIndex(AddressBookEntryDocument document)
        {
            _lastNameIndex.Add(document.LastName, document.Id.ToString());
        }

        public int Count()
        {
            return _dataStore.Count;
        }

        public AddressBookEntryDocument GetById(Guid id)
        {
            //Todo: Probably not needed
            if (id == null)
                throw new ArgumentNullException("Store engine can not retrieve document with null id");

            return _dataStore.ContainsKey(id) ? _dataStore[id] : null;
        }

        public AddressBookEntryDocument GetByPosition(int index)
        {
            if (index >= _dataStore.Count)
                throw new IndexOutOfRangeException("Can not retriece document out of range index.");
            
            var doc = _dataStore.Values[index];
            
            return new AddressBookEntryDocument(doc.Id,doc.FirstName,doc.LastName,doc.Type,doc.PhoneNumber);
        }
        
        public AddressBookEntryDocument GetByPositionOnFirstNameIndex(int index)
        {
            var guid = _firstNameIndex.GetByPosition(index);
            return guid != null ? GetById(Guid.Parse(guid)) : null;
        }
        
        public AddressBookEntryDocument GetByPositionOnLastNameIndex(int index)
        {
            var guid = _lastNameIndex.GetByPosition(index);
            return guid != null ? GetById(Guid.Parse(guid)) : null;
        }
        
    }
}
