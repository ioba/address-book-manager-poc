﻿using System;

namespace AddressBook.InternalEngine
{
    public interface IGuidService
    {
        Guid GenerateGuid();
    }
}