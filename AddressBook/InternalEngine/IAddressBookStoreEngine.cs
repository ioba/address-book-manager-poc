﻿namespace AddressBook.InternalEngine
{
    public interface IAddressBookStoreEngine
    {
        AddressBookEntryDocument StoreDocument(AddressBookEntryDocument document);
        AddressBookEntryDocument GetByPosition(int index);
        int Count();
        AddressBookEntryDocument GetByPositionOnFirstNameIndex(int index);
        AddressBookEntryDocument GetByPositionOnLastNameIndex(int index);
    }
}