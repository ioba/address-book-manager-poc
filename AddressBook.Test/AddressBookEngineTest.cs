﻿using System;
using System.Runtime.InteropServices.ComTypes;
using System.Security;
using AddressBook.Exceptions;
using AddressBook.InternalEngine;
using Moq;
using Xunit;
using Moq.Protected;
namespace AddressBook
{
    public class AddressBookEngineTest
    {
        [Fact]
        public void CreateEntry_When_FirstNameIsNull_Throws_FirsNameNullOrEmptyException()
        {
            var engine = new AddressBookEngine();

            Assert.Throws<FirstNameNullOrEmptyException>(() =>
                engine.CreateEntry(null, "MyLastName", AddressBookEntryType.Cellphone, "012345677987"));

        }
        
        
        [Fact]
        public void CreateEntry_When_FirstNameIsEmpty_Throws_FirsNameNullOrEmptyException()
        {
            var engine = new AddressBookEngine();

            Assert.Throws<FirstNameNullOrEmptyException>(() =>
                engine.CreateEntry("", "MyLastName", AddressBookEntryType.Cellphone, "012345677987"));

        }
        
        [Fact]
        public void CreateEntry_When_FirstNameIsSpaces_Throws_FirsNameNullOrEmptyException()
        {
            var engine = new AddressBookEngine();

            Assert.Throws<FirstNameNullOrEmptyException>(() =>
                engine.CreateEntry("   ", "MyLastName", AddressBookEntryType.Cellphone, "012345677987"));

        }
        
        [Fact]
        public void CreateEntry_When_LastNameIsNull_Throws_LastNameNullOrEmptyException()
        {
            var engine = new AddressBookEngine();

            Assert.Throws<LastNameNullOrEmptyException>(() =>
                engine.CreateEntry("MyFirstName", null, AddressBookEntryType.Cellphone, "012345677987"));

        }
        
        [Fact]
        public void CreateEntry_When_LastNameIsEmpty_Throws_LastNameNullOrEmptyException()
        {
            var engine = new AddressBookEngine();

            Assert.Throws<LastNameNullOrEmptyException>(() =>
                engine.CreateEntry("MyFirstName", "", AddressBookEntryType.Cellphone, "012345677987"));

        }
        
        [Fact]
        public void CreateEntry_When_LastNameIsSpaces_Throws_LastNameNullOrEmptyException()
        {
            var engine = new AddressBookEngine();

            Assert.Throws<LastNameNullOrEmptyException>(() =>
                engine.CreateEntry("MyFirstName", "  ", AddressBookEntryType.Cellphone, "012345677987"));

        }
        
        [Fact]
        public void CreateEntry_When_PhoneNumberIsNull_Throws_PhoneNumberNullOrEmptyException()
        {
            var engine = new AddressBookEngine();

            Assert.Throws<PhoneNumberNullOrEmptyException>(() =>
                engine.CreateEntry("MyFirstName", "MyLastName", AddressBookEntryType.Cellphone, null));

        }
        
        [Fact]
        public void CreateEntry_When_PhoneNumberIsEmpty_Throws_PhoneNumberNullOrEmptyException()
        {
            var engine = new AddressBookEngine();

            Assert.Throws<PhoneNumberNullOrEmptyException>(() =>
                engine.CreateEntry("MyFirstName", "MyLastName", AddressBookEntryType.Cellphone, ""));

        }
        
        [Fact]
        public void CreateEntry_When_PhoneNumberIsSpaces_Throws_PhoneNumberNullOrEmptyException()
        {
            var engine = new AddressBookEngine();

            Assert.Throws<PhoneNumberNullOrEmptyException>(() =>
                engine.CreateEntry("MyFirstName", "MyLastName", AddressBookEntryType.Cellphone, "   "));

        }
        
        [Fact]
        public void CreateEntry_When_AllPatametersAreValid_Returns_TheNewlyCreatedAddressBookEntry()
        {
            //var engine = new AddressBookEngine();
            //var createdEntry = engine.createEntry("MyFirstName", "MyLastName", AddressBookEntryType.Cellphone, "+123456789");

            var id = Guid.NewGuid();
            var entryDocument = new AddressBookEntryDocument(id, "MyFirstName", "MyLastName",
                AddressBookEntryType.Cellphone, "+123456789");

            ///Todo: Fix that test to test what is passed in storeDocument find a way to to change IsAny probably with verify
            var mockStorage = new Mock<IAddressBookStoreEngine>();
            mockStorage.Setup(stor => stor.StoreDocument(It.IsAny<AddressBookEntryDocument>()))
                .Returns(entryDocument);
            
            var mockGuid = new Mock<IGuidService>();
            mockGuid.Setup(guid => guid.GenerateGuid())
                .Returns(id);
           
            var engine = new AddressBookEngine(mockStorage.Object, mockGuid.Object);
           
            var createdEntry = engine.CreateEntry("MyFirstName", "MyLastName", AddressBookEntryType.Cellphone, "+123456789");
            Assert.NotNull(createdEntry.Id);
            Assert.Equal(createdEntry.Id,id.ToString());
            Assert.Equal(createdEntry.FirstName, "MyFirstName");
            Assert.Equal(createdEntry.LastName, "MyLastName");
            Assert.Equal(createdEntry.Type, AddressBookEntryType.Cellphone);
            Assert.Equal(createdEntry.PhoneNumber, "+123456789");
        }

        [Fact]
        public void UpdateEntry_When_AddressBookEntryIsNull_Throws_ArgumentNullException()
        {
         
            var engine = new AddressBookEngine();

            Assert.Throws<ArgumentNullException>(() =>
                engine.UpdateEntry(null));
        }

        [Fact]
        public void UpdateEntry_When_AddressBookHaveNullId_Throws_AddressBookNullIdException()
        {
            var engine = new AddressBookEngine();

            Assert.Throws<AddressBookEntryNullIdException>(() =>
                engine.UpdateEntry(new AddressBookEntry(null, "MyFirstName", "MyLastName",
                    AddressBookEntryType.Cellphone, "+123456879")));
        }
    }
}