﻿using System;
using System.Net.NetworkInformation;
using AddressBook.InternalEngine;
using Xunit;

namespace AddressBook.Test
{
    public class AddressBookEngineMapperTest
    {
        [Fact]
        public void MapToAddressBookEntry_When_AppliedToValidAddressBookEntryDocument_ReturnsAValidAddressBookEntryObject()
        {
            var id = Guid.NewGuid();
            var entryDocument = new AddressBookEntryDocument(id, "MyFirstName", "MyLastName",
                AddressBookEntryType.Cellphone, "+123456789");

            var entry = entryDocument.MapToAddressBookEntry();
            
            Assert.Equal(entryDocument.Id.ToString(), entry.Id);
            Assert.Equal(entryDocument.FirstName, entry.FirstName);
            Assert.Equal(entryDocument.LastName, entry.LastName);
            Assert.Equal(entryDocument.Type, entry.Type);
            Assert.Equal(entryDocument.PhoneNumber, entry.PhoneNumber);
            
        }

        [Fact]
        public void
            MappToAddressBookEntryDocument_When_AppliedToValidAddressBookEntry_ReturnsAValidAddressBookEntryDocument()
        {
            var id = Guid.NewGuid().ToString();
            var entry = new AddressBookEntry(id, "MyFirstName", "MyLastName",
                AddressBookEntryType.Cellphone, "+123456789");
            var entryDocument = entry.MapToAddressBookEntry();
            
            Assert.Equal(entry.Id, entryDocument.Id.ToString());
            Assert.Equal(entry.FirstName, entryDocument.FirstName);
            Assert.Equal(entry.LastName, entryDocument.LastName);
            Assert.Equal(entry.Type, entryDocument.Type);
            Assert.Equal(entry.PhoneNumber, entryDocument.PhoneNumber);
        }
    }
}