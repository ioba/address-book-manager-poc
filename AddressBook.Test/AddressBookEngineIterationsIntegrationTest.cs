﻿using Xunit;

namespace AddressBook.Test
{
    public class AddressBookEngineIterationsIntegrationTest
    {
        [Fact]
        public void IterationsTest()
        {  
            var engine = new AddressBookEngine();

            engine.CreateEntry("FirstName1","LastName1", AddressBookEntryType.Cellphone,"+111111111");
            engine.CreateEntry("FirstName2","LastName2", AddressBookEntryType.Home,"+222222222");
            engine.CreateEntry("FirstName3","LastName3", AddressBookEntryType.Work,"+333333333");

            var iterator = engine.IterateEntriesByFirstName();

            var first = iterator.FirstItem;
            var second = iterator.NextItem;
            var third = iterator.NextItem;
            
        }
    }
}