using System;
using Xunit;
using AddressBook;

namespace AddressBook.Test
{
    public class AddressBookEntryTest
    {
        [Fact]
        public void AddressBookEntry_ProperInitialized_HaveProperValues()
        {
            var entry = new AddressBookEntry("1", "MyFirstName", "MyLastName", AddressBookEntryType.Cellphone, "6978123456" );
            
            Assert.Equal(entry.Id,"1");
            Assert.Equal(entry.FirstName,"MyFirstName");
            Assert.Equal(entry.LastName, "MyLastName");
            Assert.Equal(entry.Type,AddressBookEntryType.Cellphone);
            Assert.Equal(entry.PhoneNumber, "6978123456");
        }
        
    }
  
}