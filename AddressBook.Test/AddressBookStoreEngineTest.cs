﻿using System;
using AddressBook.InternalEngine;
using Xunit;

namespace AddressBook.Test
{
    public class AddressBookStoreEngineTest
    {
        [Fact]
        public void StoreDocument_When_DocumentIsNull_Throws_ArgumentNullException()
        {
            var engine = new AddressBookStoreEngine();
            
            Assert.Throws<ArgumentNullException>(() =>engine.StoreDocument(null));
        }

        [Fact]
        public void StoreDocument_When_DocumentIsValid_Saves_DocumentInInternalStorage()
        {
            var engine = new AddressBookStoreEngine();
            var id = Guid.NewGuid();
            var document = new AddressBookEntryDocument(id, "MyFirstName", 
                "MyLastName", AddressBookEntryType.Cellphone, "+123456789");
            
            engine.StoreDocument(document);
            var result = engine.GetById(id);
            
            Assert.Equal(engine.Count(),1);
            Assert.NotNull(result);
            Assert.Equal(result.Id, document.Id);
            Assert.Equal(result.FirstName, document.FirstName);
            Assert.Equal(result.LastName, document.LastName);
            Assert.Equal(result.Type, document.Type);
            Assert.Equal(result.PhoneNumber, document.PhoneNumber);
        }
        
        [Fact]
        public void StoreDocument_When_AddThreeValidDocuments_Saves_AllThreeDocumentInInternalStorage()
        {
            var engine = new AddressBookStoreEngine();
            var id1 = Guid.NewGuid();
            var document1 = new AddressBookEntryDocument(id1, "MyFirstName", 
                "MyLastName", AddressBookEntryType.Cellphone, "+123456789");
            engine.StoreDocument(document1);
            
            var id2 = Guid.NewGuid();
            var document2 = new AddressBookEntryDocument(id2, "MyFirstName2", 
                "MyLastName2", AddressBookEntryType.Home, "+222222222");
            engine.StoreDocument(document2);
            
            var id3 = Guid.NewGuid();
            var document3 = new AddressBookEntryDocument(id3, "MyFirstName3", 
                "MyLastName3", AddressBookEntryType.Work, "+333333333");
            engine.StoreDocument(document3);
            
            Assert.Equal(engine.Count(),3);
            
            var result1 = engine.GetById(id1);
            Assert.NotNull(result1);
            Assert.Equal(result1.Id, document1.Id);
            Assert.Equal(result1.FirstName, document1.FirstName);
            Assert.Equal(result1.LastName, document1.LastName);
            Assert.Equal(result1.Type, document1.Type);
            Assert.Equal(result1.PhoneNumber, document1.PhoneNumber);
            
            var result2 = engine.GetById(id2);
            Assert.NotNull(result2);
            Assert.Equal(result2.Id, document2.Id);
            Assert.Equal(result2.FirstName, document2.FirstName);
            Assert.Equal(result2.LastName, document2.LastName);
            Assert.Equal(result2.Type, document2.Type);
            Assert.Equal(result2.PhoneNumber, document2.PhoneNumber);
            
            var result3 = engine.GetById(id3);
            Assert.NotNull(result3);
            Assert.Equal(result3.Id, document3.Id);
            Assert.Equal(result3.FirstName, document3.FirstName);
            Assert.Equal(result3.LastName, document3.LastName);
            Assert.Equal(result3.Type, document3.Type);
            Assert.Equal(result3.PhoneNumber, document3.PhoneNumber);
        }

        [Fact]
        public void StoreDocument_When_AddingTwoDiffDocumentsWithTheSameGuid_Then_OnlyTheLastOneRemains()
        {
            var engine = new AddressBookStoreEngine();
            var id1 = Guid.NewGuid();
            var document1 = new AddressBookEntryDocument(id1, "MyFirstName", 
                "MyLastName", AddressBookEntryType.Cellphone, "+123456789");
            engine.StoreDocument(document1);
            
            
            var document2 = new AddressBookEntryDocument(id1, "MyFirstName2", 
                "MyLastName2", AddressBookEntryType.Home, "+222222222");
            engine.StoreDocument(document2);
            
            
            Assert.Equal(engine.Count(),1);
           
            var result = engine.GetById(id1);
            Assert.NotNull(result);
            Assert.Equal(result.Id, document2.Id);
            Assert.Equal(result.FirstName, document2.FirstName);
            Assert.Equal(result.LastName, document2.LastName);
            Assert.Equal(result.Type, document2.Type);
            Assert.Equal(result.PhoneNumber, document2.PhoneNumber);
            
        }

        [Fact]
        public void GetById_When_IdDoesNotBelongToADocument_Then_ReturnsNull()
        {
            var engine = new AddressBookStoreEngine();
            var id1 = Guid.NewGuid();
            var document1 = new AddressBookEntryDocument(id1, "MyFirstName", 
                "MyLastName", AddressBookEntryType.Cellphone, "+123456789");
            engine.StoreDocument(document1);
            
            var id2 = Guid.NewGuid();
            var document2 = new AddressBookEntryDocument(id2, "MyFirstName2", 
                "MyLastName2", AddressBookEntryType.Home, "+222222222");
            engine.StoreDocument(document2);
            
            var id3 = Guid.NewGuid();
            var document3 = new AddressBookEntryDocument(id3, "MyFirstName3", 
                "MyLastName3", AddressBookEntryType.Work, "+333333333");
            engine.StoreDocument(document3);
            
            Assert.Equal(3,engine.Count());

            var randomId = Guid.NewGuid();
            var result = engine.GetById(randomId);
            
            Assert.Null(result);
        }

    }
}